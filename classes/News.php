<?php

    class News {
        
        public $connection;
      
        private $id;
        
        public function __construct($connection){
            $this->connection = $connection;
        }        
  
        
        public function loadNews($id = 0, $limit = 1){

            if(!$id) {$id = "%"; }
            
           $query = $this->connection->query('SELECT * FROM cms_news WHERE id LIKE :id ORDER BY id DESC LIMIT ' . $limit, [
               'id' => $id
           ]);
            
           $data = [];
           
           while($row = $query->fetch_assoc()){
               array_push($data, $row);
           }

           return $data;
        }
        
        public function checkNewsId($id){    
		
			$newsid = $id;
			
			if(is_numeric($newsid)) {
				$select = $this->connection->query('SELECT * FROM cms_news WHERE id=:id', [
					"id" => $newsid
				]);
				
				if($select->num_rows) {
					return true;
				}
			}
			
			return false;
        }
        
        public function getNewsData($id){
            $query = $this->connection->query("SELECT * FROM cms_news WHERE id LIKE :id ", [
                'id' => $id
            ]);
            
            $data = [];
            
            while($row = $query->fetch_assoc()){
                array_push($data, $row);
            }
            
            return $data;
        }
        
        public function newsTags($newsid){
            $id = $this->connection->query("SELECT * FROM news_tags WHERE newsid LIKE :newsid ", [
               'newsid' => $newsid 
            ]);
            
            $data = [];
            
            while($row = $id->fetch_assoc()){
                array_push($data, $row);
            }
            
            return $data;
            
        }
        
        public function insertComment($username, $comment, $send, $id) {
            if(isset($send)){
                if(!empty($username) AND !empty($comment)){
                    $query = $this->connection->query("INSERT INTO news_comments SET newsid = :id, username = :username, comment = :comment, date = :date", [
                       'id' => $id,
                        'username' => $username,
                        'comment' => $comment,
                        'date' => date("d.m.Y")
                    ]);
                } else {
                    return 'Hallo';
                }
            }
        }
        
        public function getComment($id){
            $query = $this->connection->query("SELECT * FROM news_comments WHERE newsid = :id", [
                'id' => $id
            ]);
            
            $data = [];
            
            while($row = $query->fetch_assoc()){
                array_push($data, $row);
            }
            
            return $data;
            
        }
    }

?>
