<?php
final class General {
	
	public $connection;
	
	public function __construct($con) {
		$this->connection = $con;
	}
	
	public function statistic($table, $clause = "", $params = "") {
		$count = $this->connection->query('SELECT * FROM ' . $table . ' ' . $clause, $params)->num_rows;
		return $count;
	}
}

?>