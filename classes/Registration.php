<?php
final class Registration {
	public $connection;
	public $errors = [];
	public $forms = ['email','username','password','password_re','c_captcha'];
	
	public function __construct($connection) {
		$this->connection = $connection;
		
		foreach($this->forms as $name) {
			if(isset($_POST[$name])) {
				$this->forms[$name] = htmlspecialchars($_POST[$name]);
			} else {
				$this->forms[$name] = '';
			}
		}

		if(isset($_POST["submit"])) {
			if(!isset($_POST['newsletter'])) {
				$_POST['newsletter'] = 0;
			}
			
			if(!isset($_POST['tos'])) {
				$_POST['tos'] = 0;
			}
			
			if($this->validate($_POST['username'], $_POST['email'], $_POST['password'], $_POST['password_re'], $_POST['tos'], $_POST['c_captcha'], $_POST['g-recaptcha-response'], $_POST['newsletter'])) {
				$this->execute($_POST['username'], $_POST['email'], $_POST['password'], $_POST['newsletter']);
			}
		}
	}
	
	public function showError() {
		if(!empty($this->errors)) {
			echo '<div class="form-error">' . $this->errors[0] . '</div>';
		}
	}
	
	public function validate($username, $email, $password, $password_re, $tos, $c_captcha, $g_captcha, $newsletter) {
		$error_msgs = [
			"username" => 
				["short" => "Der Username ist zu kurz (mind. 3 Zeichen).",
				 "long" => "Der Username ist zu lang (max. 25 Zeichen).",
				 "extising" => "Der Username existiert bereits.",
				 "whitespaces" => "Der Username darf keine Leerzeichen enthalten."],
			"email" => 
				["invalid" => "Die E-Mail ist ungültig.",
				 "extising" => "Die E-Mail Adresse existiert bereits.",
				 "long" => "Die E-Mail ist zu lang (max. 120 Zeichen)."],
			"password" => 
				["short" => "Das Passwort ist zu kurz (mind. 6 Zeichen).",
				 "long" => "Das Passwort ist zu lang (max. 30 Zeichen)."], 
			"password_re" => 
				["equal" => "Die Passwörter stimmen nich überein."], 
			"c_captcha" => 
				["invalid" => "Verdächtiges Verhalten wurde erfasst. Bitte erneut versuchen."], 
			"g_captcha" => 
				["validate" => "Bitte validiere das Captcha",
				 "invalid" => "Das Captcha ist ungültig. Bitte erneut versuchen."], 
			"tos" => 
				["accept" => "Du musst die AGBs akzeptieren."]
			
		];
		
		# username
			$username = trim(htmlspecialchars(($username)));
			$username_rep = str_replace(" ", "", $username);
			
			if(!strlen($username) >= 3) {
				array_push($this->errors, $error_msgs["username"]["short"]);
			}
			
			if(strlen($username) > 25) {
				array_push($this->errors, $error_msgs["username"]["long"]);
			}
			
			if($username != $username_rep) {
				array_push($this->errors, $error_msgs["username"]["whitespaces"]);
			}
			
			$extising = $this->connection->query('SELECT * FROM users WHERE username = :username', [
				"username" => $username
			])->num_rows;

			if($extising) {
				array_push($this->errors, $error_msgs["username"]["extising"]);
			}
		
		# email
			$email = trim(htmlspecialchars($email));
			
			if(strlen($email) > 120) {
				array_push($this->errors, $error_msgs["email"]["long"]);
			}
			
			if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				array_push($this->errors, $error_msgs["email"]["invalid"]);
			}
			
			$extising = $this->connection->query('SELECT * FROM users WHERE email = :email', [
				"email" => $email
			])->num_rows;
			
			if($extising) {
				array_push($this->errors, $error_msgs["email"]["extising"]);
			}
			
		# password
			$password = trim(htmlspecialchars($password));
			$password_re = trim(htmlspecialchars($password_re));	
					
			if(!strlen($password) >= 6) {
				array_push($this->errors, $error_msgs["password"]["short"]);
			}
			
			if(strlen($password) > 30) {
				array_push($this->errors, $error_msgs["password"]["long"]);
			}
			
			if($password != $password_re) {
				array_push($this->errors, $error_msgs["password_re"]["equal"]);
			}
			
		# tos
			if(!isset($tos)) {
				array_push($this->errors, $error_msgs["tos"]["accept"]);
			}
			
			if(!$tos) {
				array_push($this->errors, $error_msgs["tos"]["accept"]);
			}
			
		# newsletter
			if($newsletter) {
				$newsletter = 1;
			} else {
				$newsletter = 0;
			}
			
		# captcha
			$c_captcha = trim(htmlspecialchars($c_captcha));
			$g_captcha = trim(htmlspecialchars($g_captcha));
			if(!empty($c_captcha)) {
				array_push($this->errors, $error_msgs["c_captcha"]["invalid"]);
			}
			
			if(!isset($g_captcha)) {
				array_push($this->errors, $error_msgs["g_captcha"]["validate"]);
			}
			
			if(!$g_captcha) {
				array_push($this->errors, $error_msgs["g_captcha"]["invalid"]);
			} else {
				$response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcrBhUTAAAAAC43AcqGOt2kOJNS8L6PDqBbTnn1&response=".$g_captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
				if($response['success'] == false) {
				  array_push($this->errors, $error_msgs["g_captcha"]["invalid"]);
				}
			}
			
			
		# return
		if(!empty($this->errors)) {
			return false;
		}
		
		return true;
	}
	
	public function execute($username, $email, $password, $newsletter) {
		$username = trim(htmlspecialchars($username));
		$password = hash("sha224", md5(trim(htmlspecialchars($password))));
		$email = trim(htmlspecialchars($email));
		$ip = $_SERVER['REMOTE_ADDR'];
		
		$this->connection->query('INSERT INTO users (username, password, email, newsletter, ip) VALUES (:username, :password, :email, :newsletter, :ip)', [
			"username" => $username,
			"password" => $password,
			"email" => $email,
			"newsletter" => $newsletter,
			"ip" => $ip
		]);
		
		$login = new SecurityLogin($this->connection);
		
		$userid = $this->connection->query('SELECT * FROM users WHERE username = :username', [
			"username" => $username
		])->fetch_assoc();
		
		$this->connection->query('INSERT INTO cms_security_login (user_id, security_id, session_id, expire, ipaddress) VALUES (:user_id, :security_id, :session_id, :expire, :ipaddress)', [
			"user_id" => $userid["id"],
			"security_id" => $_SESSION['securityid'],
			"session_id" => $_SESSION['sessionid'],
			"expire" => $login->cookie_time,
			"ipaddress" => $ip
		]);
		
		header('Location: ' . Config::PATH);

	}
}
?>