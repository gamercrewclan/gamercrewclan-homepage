<?php

class User {
    private $row;
    private $connection;
    private $userManager;
    
    public function __construct($connection, $id) { 
        $this->connection = $connection;
        $this->userManager = new UserManager($connection);
		$this->row = $this->userManager->getRow($id); 
	}
    
    public function getUsername() {
        return $this->row->username;
    }
    
    public function getID() {
        return $this->row->id;
    }
    
    public function getPassword() {
        return $this->row->password;
    }
    
    public function getRank() {
        return $this->row->rank;
    }
}