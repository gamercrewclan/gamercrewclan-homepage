<?php

final class Config {
	
	# page
	const PATH = 'http://127.0.0.1/bitbucket/gamercrewclan-homepage';
	const PAGE_NAME = 'GamerCrewClan';
	
	# mysql
	const MYSQL_HOST = '127.0.0.1';
	const MYSQL_USER = 'root';
	const MYSQL_PASSWORD = '';
	const MYSQL_DATABASE = 'gcc';
	const MYSQL_PORT = 3306; 
		
	# php
	const PHP_PATH = 'E:\xampp\htdocs\bitbucket\gamercrewclan-homepage';
	
	# template 
	const TEMPLATE_PATH = Config::PHP_PATH . '\templates';
}

