<?php 

    class UserManager {
        
        private $connection;
        
        private $loadedUsers = [];
        
        public function __construct($connection){
            
            $this->connection = $connection;
            
        }
		
		public function getRow($id) {
			$row = $this->connection->query('SELECT * FROM users WHERE `id` = :id', [
               'id' => $id 
            ]);
			return $row->fetch_object();
		}
			
        
        public function getById($id){
            return $this->getByColumn('id', $id);
        }
        
        private function getByColumn($col, $val) {
            $query = $this->connection->query('SELECT * FROM users WHERE `'.$col.'` = :val', [
               'val' => $val 
            ]);
            
            if($query->num_rows == 0)
                return null;
           
            $row = $query->fetch_object();
            
            if(isset($this->loadedUsers[$row->id]))
                return $this->loadedUsers[$row->id];
            
            $user = new User($row, $this->connection, $this);
            $this->loadedUsers[$row->id] = $user;
            
            return $user;
        }
        
    }