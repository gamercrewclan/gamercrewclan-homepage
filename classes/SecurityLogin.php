<?php 

    class SecurityLogin  {
        
        private $connection;
		public $cookie_time;
		
       	public function __construct($connection){
            $this->connection = $connection;
			$this->cookie_time = time()+60*60;
            
            if(!isset($_SESSION['sessionid'])){
                session_regenerate_id();
                $_SESSION['sessionid'] = session_id();
            }
            
             if(!isset($_SESSION['securityid'])){
                $_SESSION['securityid'] = hash('sha224', md5(session_id()));
            }
            
        }
        
        public function IsLoggedIn(){
            
            if(isset($_SESSION['id'], $_SESSION['sessionid'], $_SESSION['securityid'])) {
                $query = $this->connection->query('SELECT * FROM cms_security_login WHERE user_id = :id, session_id = :sessionid, security_id = :securityid', [
                   'id' => $_SESSION['id'],
                    'sessionid' => $_SESSION['id'], 
                    'securityid' => $_SESSION['securityid']
                ]);
              
                
                if($query->num_rows){
                    $row = $query->fetch_assoc();
                    if(time() < $row['expire']){
                        if($row['cookies']){
                           setcookie('id', $_SESSION['id'], time()+60*60);
                           setcookie('securityid', $_SESSION['securityid'], time()+60*60);
                           setcookie('sessionid', $_SESSION['sessionid'], time()+60*60); 
                        }
                        return true;
                    } else {
                        $this->Logout();
                    }
                }  
                
            } else if(isset($_COOKIE['id'], $_COOKIE['securityid'], $_COOKIE['sessionid'])){
                $query = $this->connection->query('SELECT * FROM cms_security_login WHERE user_id = :id, session_id = :sessionid, security_id = :securityid', [
                   'id' => $_COOKIE['id'],
                    'sessionid' => $_COOKIE['id'], 
                    'securityid' => $_COOKIE['securityid']
                ]);
              
                
                if($query->num_rows){
                    $row = $query->fetch_assoc();
                    if(time() < $row['expire']){
                        $_SESSION['id'] = $_COOKIE['id'];
                        $_SESSION['sessionid'] = $_COOKIE['sessionid'];
                        $_SESSION['securityid'] = $_COOKIE['securityid'];
                        return true;
                    } else {
                        $this->Logout();
                    }
                }  
            } return false;               
        }
        
        public function Logout(){
			
			$this->connection->query('DELETE FROM cms_security_login WHERE user_id = :id, session_id = :sessionid, security_id = :securityid', [
                   'id' => $_SESSION['id'],
                    'sessionid' => $_SESSION['sessionid'], 
                    'securityid' => $_SESSION['securityid']
                ]);
			
            $_SESSION['id'] = '';
            $_SESSION['securityid'] = '';
            $_SESSION['sessionid'] = '';
            
            setcookie('id', $_SESSION['id'], -1);
            setcookie('securityid', $_SESSION['securityid'], -1);
            setcookie('sessionid', $_SESSION['sessionid'], -1); 
            
            session_destroy();
        }
        
    }


?>