<?php

	session_start();
	
    # error reporting
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
    
    # core
    require_once 'classes/Config.php';
    require_once 'classes/autoload.php';
    $connection = new Connection(new MySQLi(Config::MYSQL_HOST,Config::MYSQL_USER,Config::MYSQL_PASSWORD,Config::MYSQL_DATABASE,Config::MYSQL_PORT));
	
	$login = new SecurityLogin($connection);
	
    $templateHandler = new TemplateHandler(Config::TEMPLATE_PATH);
	
	# routing
    $route = new Route();
    $route->setTemplateHandler($templateHandler);
	# load template
			
	# login:
	# 0 - Für alle
	# 1 - Only User
	# 2 - Only Guests
	$sites = $connection->query('SELECT * FROM cms_pages WHERE admin = 0');
	
	while($site = $sites->fetch_assoc()) {
		$template_query = $connection->query('SELECT * FROM cms_templates WHERE id = :id', [
			'id' => $site['template_id']
		])->fetch_assoc();
		$logif = [1, $login->IsLoggedIn(), !$login->IsLoggedIn()];
		
		if($logif[$site['login']]) {
			$route->add($template_query['route'], function(TemplateHandler $templateHandler) use($template_query) {
				$tpl = $templateHandler->getTemplate($template_query['template'] . '.tpl.php');
				//$tpl->connetion = $connection;
				$tpl->display(true);
			});	
		}
	}
	
	$route->submit();

?>
