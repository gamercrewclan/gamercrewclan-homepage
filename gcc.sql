/*
Navicat MySQL Data Transfer

Source Server         : Habbo
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : gcc

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-01-30 14:56:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `category`
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', 'Games');
INSERT INTO `category` VALUES ('2', 'GamerCrewClan');
INSERT INTO `category` VALUES ('3', 'Zelda');

-- ----------------------------
-- Table structure for `cms_news`
-- ----------------------------
DROP TABLE IF EXISTS `cms_news`;
CREATE TABLE `cms_news` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `subtext` text,
  `content` text,
  `date` varchar(255) DEFAULT NULL,
  `image` text,
  `category` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cms_news
-- ----------------------------
INSERT INTO `cms_news` VALUES ('1', 'Test1', 'wir testen den subtext', 'Hallo bitch.', '15.01.2016', 'http://wallpapercave.com/wp/sMZgFbt.jpg', null, '1', null);
INSERT INTO `cms_news` VALUES ('2', 'Test2', null, null, '15.01.2016', 'http://www.download-free-wallpaper.com/img28/jjivbvkqfvyoknfaeyxn.jpg', null, null, null);
INSERT INTO `cms_news` VALUES ('5', 'Test3', null, null, '15.01.2016', 'https://a8b9696a91de10cc2c20196ef7b687396774edd5.googledrive.com/host/0B0RTlVpNFM-GfkRsWVdJOXJaZkJIQXV5OXRhcDZEVFBfb0ZMak9TOWJhNnFEZ1ZYZ0Y0blU/dark-souls-wallpaper-picture-FfPH.jpg', null, null, null);
INSERT INTO `cms_news` VALUES ('6', 'Test4', null, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', '15.01.2016', 'http://img00.deviantart.net/74ab/i/2014/056/7/f/cod_ghosts_wallpaper_by_kingdoesstuff-d77xdc1.jpg', null, null, 'Holyfuture');

-- ----------------------------
-- Table structure for `cms_pages`
-- ----------------------------
DROP TABLE IF EXISTS `cms_pages`;
CREATE TABLE `cms_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `link` varchar(255) NOT NULL DEFAULT '',
  `rank` int(11) DEFAULT '0',
  `template_id` int(11) NOT NULL DEFAULT '0',
  `headnavi` int(10) NOT NULL DEFAULT '0',
  `login` int(11) NOT NULL DEFAULT '0',
  `admin` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cms_pages
-- ----------------------------
INSERT INTO `cms_pages` VALUES ('1', 'home', '', '0', '1', '1', '0', '0');
INSERT INTO `cms_pages` VALUES ('2', 'teamspeak', 'ts3server://gamercrew.1337.cf/', '0', '0', '1', '0', '0');
INSERT INTO `cms_pages` VALUES ('3', 'Youtube', 'https://www.youtube.com/channel/UCVs49k4RJCIFg9Ef4kVLQDA', '0', '0', '1', '0', '0');
INSERT INTO `cms_pages` VALUES ('4', 'Anmeldung', '', '0', '2', '0', '2', '0');
INSERT INTO `cms_pages` VALUES ('5', 'login', '', '0', '3', '0', '2', '0');
INSERT INTO `cms_pages` VALUES ('6', 'news', '/news', '0', '4', '0', '0', '0');
INSERT INTO `cms_pages` VALUES ('7', 'Suche', '/search', '0', '5', '1', '0', '0');

-- ----------------------------
-- Table structure for `cms_security_login`
-- ----------------------------
DROP TABLE IF EXISTS `cms_security_login`;
CREATE TABLE `cms_security_login` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `security_id` varchar(255) DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL,
  `ipaddress` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `cookies` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cms_security_login
-- ----------------------------
INSERT INTO `cms_security_login` VALUES ('3', '156cd7f0f0d3b3017e6ce15908d39bfc4573e620f70ec9a20cdac012', 'gth8534i00633ubh76nkefgpa6', '1452639650', '127.0.0.1', '1', '0');
INSERT INTO `cms_security_login` VALUES ('4', '156cd7f0f0d3b3017e6ce15908d39bfc4573e620f70ec9a20cdac012', 'gth8534i00633ubh76nkefgpa6', '1452640047', '127.0.0.1', '2', '0');
INSERT INTO `cms_security_login` VALUES ('5', '156cd7f0f0d3b3017e6ce15908d39bfc4573e620f70ec9a20cdac012', 'gth8534i00633ubh76nkefgpa6', '1452714021', '127.0.0.1', '2', '0');
INSERT INTO `cms_security_login` VALUES ('6', '156cd7f0f0d3b3017e6ce15908d39bfc4573e620f70ec9a20cdac012', 'gth8534i00633ubh76nkefgpa6', '1452714105', '127.0.0.1', '2', '0');
INSERT INTO `cms_security_login` VALUES ('7', '156cd7f0f0d3b3017e6ce15908d39bfc4573e620f70ec9a20cdac012', 'gth8534i00633ubh76nkefgpa6', '1452714190', '127.0.0.1', '2', '0');
INSERT INTO `cms_security_login` VALUES ('8', '156cd7f0f0d3b3017e6ce15908d39bfc4573e620f70ec9a20cdac012', 'gth8534i00633ubh76nkefgpa6', '1452714231', '127.0.0.1', '2', '0');
INSERT INTO `cms_security_login` VALUES ('9', 'b5db9043b218f6ffeb7c6d3aa5cb4ce0654ae5817ee23cbc3d9c82f5', '9gp4gveuhu77j1g84qcr1pm4c6', '1452716602', '127.0.0.1', '1', '0');
INSERT INTO `cms_security_login` VALUES ('10', 'b5db9043b218f6ffeb7c6d3aa5cb4ce0654ae5817ee23cbc3d9c82f5', '9gp4gveuhu77j1g84qcr1pm4c6', '1452718944', '127.0.0.1', '2', '0');

-- ----------------------------
-- Table structure for `cms_templates`
-- ----------------------------
DROP TABLE IF EXISTS `cms_templates`;
CREATE TABLE `cms_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template` varchar(255) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cms_templates
-- ----------------------------
INSERT INTO `cms_templates` VALUES ('1', 'index', '/');
INSERT INTO `cms_templates` VALUES ('2', 'register', '/register');
INSERT INTO `cms_templates` VALUES ('3', 'login', '/login');
INSERT INTO `cms_templates` VALUES ('4', 'news', '/news');
INSERT INTO `cms_templates` VALUES ('5', 'suche', '/search');

-- ----------------------------
-- Table structure for `news_comments`
-- ----------------------------
DROP TABLE IF EXISTS `news_comments`;
CREATE TABLE `news_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `date` text,
  `comment` text,
  `newsid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of news_comments
-- ----------------------------
INSERT INTO `news_comments` VALUES ('13', 'Max', '29.01.2016', 'test', '6');
INSERT INTO `news_comments` VALUES ('14', 'Holyfuture', '30.01.2016', 'geile news du affe', '6');
INSERT INTO `news_comments` VALUES ('15', 'Niko', '30.01.2016', 'Ich liebe Wieland', '6');

-- ----------------------------
-- Table structure for `news_tags`
-- ----------------------------
DROP TABLE IF EXISTS `news_tags`;
CREATE TABLE `news_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) DEFAULT NULL,
  `newsid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of news_tags
-- ----------------------------
INSERT INTO `news_tags` VALUES ('1', 'Gewinnspiel', '6');
INSERT INTO `news_tags` VALUES ('2', 'Spiele', '6');
INSERT INTO `news_tags` VALUES ('3', 'tag3', '5');
INSERT INTO `news_tags` VALUES ('4', 'GamerCrewClan', '6');
INSERT INTO `news_tags` VALUES ('5', 'Zelda', '6');
INSERT INTO `news_tags` VALUES ('6', 'Nintendo', '6');
INSERT INTO `news_tags` VALUES ('7', 'Artikel', '6');
INSERT INTO `news_tags` VALUES ('8', 'Spiele', '5');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `rank` int(11) DEFAULT '1',
  `email` varchar(255) NOT NULL,
  `newsletter` int(11) NOT NULL DEFAULT '0',
  `last_visit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip` varchar(122) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Bitchboy', '0e0589cff765d2c147a5f670ab722f92916dac8de411d508c480a337', '1', 'bitchmail@web.de', '0', '2016-01-22 15:31:16', '127.0.0.1', 'active');
INSERT INTO `users` VALUES ('2', 'Holyfuture', 'dfe4e054e943e4cc5b920f8f38bbc945317c4861100a5d431479f8d0', '1', 'w.wuller@web.de', '1', '2016-01-22 15:31:17', '127.0.0.1', 'active');
