<?php
$navigation = array();

function drawNavi() {
	global $connection;
	$all_pages = $connection->query('SELECT * FROM cms_pages');
	
	$return = '';
	
	while($row = $all_pages->fetch_assoc()) {
		$logif = [1, $login->IsLoggedIn(), !$login->IsLoggedIn()];
		
		if($logif[$site['login']]) {
			$navigation[] = $row;
		}
	}
	
	foreach($navigation as $nav) {
		$return .= '<a href="./' . $nav['link'] . '"><li>' . strtoupper($nav['name']) . '</li></a><div class="vr"></div>';
	}
	
	return $return;
}

?>

<div class="navigation">
    <div class="navigation_links">
        <div class="container">
            <ul>
                <?php echo drawNavi(); ?>
            </ul>
        </div>
    </div>
</div>