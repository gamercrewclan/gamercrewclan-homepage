<link rel="stylesheet" href="style/register.css">

<?php
	$register = new Registration($connection);
?>

<div class="container">     
    
     <div class="right">
        <div class="register_info">
            <p>Willkommen auf unserer GamerCrewClan Homepage. Du bist kurz davor dich bei uns zu registrieren. Unsere Registrierung
            ist komplett kostenfrei. Treten jetzt der GamerCrewClan Community bei! <br /><br />
            
            <b>Schalte weitere Features frei!</b><br />
            Mit der erfolgreichen Registrierung schließt du unsere AGBs ab und erhältst somit den für dich autorisierten Seitenzugriff.
            Auf unserer GamerCrewClan Webseite erhältst du jeden freigeschalteten Content und zugang zu deinem Benutzerprofil. <br /><br />
            
            <b>Unterhalte dich mit der Community!</b><br />
            Als registrierter Benutzer hast du die Möglichkeit in unserem GamerCrewClan Forum Beiträge zu schreiben, um mit anderen zu
            kommunizieren. Tausche deine Meinung mit anderen aus und diskutiere über verschiedenste Spiele. <br /><br />
            
            <b>Empfange unsere News per Mail!</b><br />
            Du kannst unseren Newsletter abonnieren, dies komplett kostenfrei! Erhalte GamerCrewClan News schnell und ohne Verzögerung
            in dein Postfach, auch wärend einer Offline-Phase. Nutzer die unseren Newsletter abonniert haben, erhalten Mails zu unseren
            Gewinnspielen. Aus Respekt deiner Privatsphäre werden wir deine E-mail nicht an Drittanbieter weiterverkaufen oder veröffentlichen.
            Du erhältst von uns keine Junk Mails, versprochen!
            </p>
        </div>
     </div>
    
    <div class="form_container">
            <h1>Registrierung</h1>        
        <div class="left">
            <div id="errorBox">
                <div id="errorMsg"><?php $register->showError(); ?></div>
            </div>

            <script>

                        $(document).ready(function(){
                                 checkOnError();
                        });

                        function checkOnError() {
                                if ($("#errorMsg > *").length > 0) {
                                        if($(".form-error").text().length > 0) {
                                                $('#errorBox').show();
                                        }
                                }
                        }


                </script>

            <script>
                        var $messages = $('#errorMsg');
                </script>

            <form id="registration" action="<?php echo Config::PATH . Route::$path; ?>" method="post">
                <input type="text" data-validation="length alphanumeric" data-validation-length="3-25" name="username" value="<?php echo $register->forms['username'];?>" placeholder="Username" class="inputStyle" data-validation-error-msg="Der Username muss 3-25 Zeichen enthalten."><br>
                <input type="email" data-validation="email" name="email" value="<?php echo $register->forms['email'];?>" placeholder="E-Mail" class="inputStyle" data-validation-error-msg="Die E-Mail ist ungültig."><br>

                <input type="password" data-validation-error-msg="Das Passwort muss 6 Zeichen enthalten." data-validation="length" data-validation-length="min6" name="password" value="<?php echo $register->forms['password'];?>" placeholder="Passwort" class="inputStyle"><br>
                <input type="password" data-validation-error-msg="Die Passwörter müssen übereinstimmen." data-validation="confirmation" data-validation-confirm="password" name="password_re" value="<?php echo $register->forms['password_re'];?>" placeholder="Passwort wiederholen" class="inputStyle"><br>

                <div id="checkbox_div" style="float:left;">
                    <div class="checkbox">
                        <input type="checkbox" id="newsletter" name="newsletter" value="1" checked>
                        <label for="newsletter"></label>
                    </div>
                    <div class="label"><label for="newsletter"><b>Newsletter</b></label></div>
                    <div class="subtext">
                        <label for="newsletter">Ja, ich möchte News & Updates erhalten.</label>
                    </div>
                </div>

                 <div id="checkbox_div" style="float:left;">
                    <div class="checkbox">
                        <input type="checkbox" id="tos" name="tos" value="1" required>
                        <label for="tos"></label>
                    </div>
                    <div class="label"><label for="tos"><b>AGBs</b></label></div>
                    <div class="subtext">
                        <label for="tos">Hiermit bestätige ich, die AGB zur Kenntnis genommen zu haben und akzeptiere diese.</label>
                    </div>
                </div>
                
                <input type="text" name="c_captcha" value="<?php echo $register->forms['c_captcha'];?>" name="custom_captcha" style="display:none;" class="inputStyle">
                <div id="captcha"><div class="g-recaptcha" data-size="200" data-theme="dark" data-sitekey="6LcrBhUTAAAAAIPRlpZ0l12zU60OTsCZLOveexW1"></div></div>
                
                <input type="submit" name="submit" value="Registrieren"><br>
                <input type="reset" name="submit" value="Reset"><br>
            </form>
        </div>
    </div>
</div>
<script>
  $.validate({
	errorMessagePosition : $messages,
	modules : "security",
	showErrorDialogs : true,
	
	success: function(label){
    	label.addClass("valid").text("");                
	}              
  });
  
	$('input').on('focusout', function() {
	 	setTimeout(function(){ checkOnError(); }, 100);
	});
</script>
