       	<div id="wrapper" class="clearfix">
        </div>
        
        <p id="back-top"><span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></p>
        <footer>
            <p><a href="<?php echo Config::PATH; ?>"><b><?php echo Config::PAGE_NAME; ?></b></a> &copy; 2016 - <u>AGBs</u> | <u>Rechtlinien</u> | <u>Impressum</u> | <u>Dankessagung</u></p>
            <p style="width:960px;">GamerCrewClan.de ist eine eigenständige Seite und unterliegt dem Recht des Impressumsinhaber! GamerCrewClan.de
            wurde von unserem GamerCrewClan konzepiert und entwickelt. Falls sie Fragen oder Probleme zur unserer Webseite haben, bitten wir Sie
            vorher unsere Informationsseite durchzulesen, um eventuelle Fragen vorwegzunehmen.</p>
        </footer>
    </div>
</body>
</html>