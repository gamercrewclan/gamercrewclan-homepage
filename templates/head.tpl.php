<!DOCTYPE html>
<html>
    <head>
        <title>GamerCrewClan - Startseite</title>
        <link rel="stylesheet" href="style/main.css">
        
        <!-- Bootstrap -->
		<link rel="stylesheet" href="style/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="style/bootstrap/css/bootstrap-theme.min.css">
        
        <!-- Glyphicons -->
        <link rel="stylesheet" href="style/bootstrap/fonts/glyphicons-halflings-regular.eot">
        <link rel="stylesheet" href="style/bootstrap/fonts/glyphicons-halflings-regular.svg">
        <link rel="stylesheet" href="style/bootstrap/fonts/glyphicons-halflings-regular.ttf">
        <link rel="stylesheet" href="style/bootstrap/fonts/glyphicons-halflings-regular.woff">
        <link rel="stylesheet" href="style/bootstrap/fonts/glyphicons-halflings-regular.woff2">

        <meta charset="UTF-8">
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>  

        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.43/jquery.form-validator.min.js"></script>
        <script src="js/slider_class.js"></script>
        <script src="js/slider_effects.js"></script>
        <?php include('includes/slider.php'); ?>
        <script src="js/slider.js"></script>
        <script src="js/tweaks.js"></script>
        
        <script src='https://www.google.com/recaptcha/api.js'></script>
        

        <script type="text/javascript">
            window.cookieconsent_options = {"message":"Diese Website nutzt Cookies um dir die beste Funktionalität freizugeben.","dismiss":"Verstanden!","learnMore":"Mehr lesen","link":"<?php echo Config::PATH . '/cookies';?>","theme":"dark-bottom"};
        </script>
        
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
    </head>
    
    <div id="fullpage">
    <div id="content">