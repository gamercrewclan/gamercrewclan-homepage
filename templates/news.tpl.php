<link rel="stylesheet" href="style/news.css">
<?php
    $checkId = new News($connection);
	
	if(!$checkId->checkNewsId($_GET['newsid'])) {
		header('Location: ' . Config::PATH);
	}
	
   	if(isset($_POST['sendcomment'])){
    	$news = new News($connection);
     	$news->insertComment($_POST['username'], $_POST['comment'], $_POST['sendcomment'], $_GET['newsid']);
    }    
?>
<div class="container">
    <div class="content">
        
        <div class="right" style="">
            <div class="commentbox">
                <p>Kommentare:</p>
                
                <?php 
                    $comment = new News($connection);
                    $data = $comment->getComment($_GET['newsid']);
                    foreach($data as $comments) {
                ?>
                <div class="comment_title">
                    <p><b><?php echo $comments['username']; ?> schrieb am <?php echo $comments['date']; ?>:</b></p>
                </div>
                
                <div class="comment_content">
                    <p><?php echo $comments['comment']; ?> </p>
                </div>
                
               <?php } ?>
            </div>
        </div>
        
        <?php 
        $news = new News($connection);
        $data = $news->getNewsData($_GET['newsid']);
        foreach($data as $article) {
        ?> 
        
        <div class="news_title"><p><?php echo $article['title']; ?></p></div>
        <div class="news_subinfo"><p><?php echo $article['date'] . ' | Kategorie: '. $article['category'] . ' | Geschrieben von: ' . $article['author']; ?></p></div>
        
        <br /><br />
        <div class="left" style="">
            <div class="news_content">
                <p><?php echo $article['content']; ?></p>
            </div>
            
            
             <?php 
                       
                $news = new News($connection);
                $data = $news->newsTags($_GET['newsid']);
                foreach($data as $tag) {
                    
            ?>
          
            
            <div class="tags">
                <div class="tagbox">
                    <p>
                      <?php echo $tag['tag']; ?> 
                    </p>
                </div>
            </div>
            
                <?php }
        
        	} ?>
            
            <div class="comment_content">
                <form method="POST">
                    <input type="text" name="username" class="user_input" placeholder="Dein Benutzername"><br /><br />
                    <textarea name="comment" class="comment_input" placeholder="Schreibe einen Kommentar"></textarea><br /><br />
                    <input type="submit" name="sendcomment" class="comment_submit" value="Kommentar abschicken">
                </form>
            </div>
            
        </div> 
    </div>
</div>