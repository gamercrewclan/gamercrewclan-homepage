<?php
$navigation = array();

function drawNavi() {
	global $navigation;
	global $connection;
	$all_pages = $connection->query('SELECT * FROM cms_pages WHERE headnavi = 1 AND admin = 0');
	
	$return = '';
	
	while($row = $all_pages->fetch_assoc()) {
		$navigation[] = $row;
	}

	if(is_array($navigation)) {
		foreach($navigation as $nav) {
			if($nav['template_id']) {
				$templ_link = $connection->query('SELECT * FROM cms_templates WHERE id = :id', [
					"id" => $nav['template_id']
				])->fetch_assoc();
				$nav['link'] = Config::PATH . $templ_link['route'];
			}
			$return .= '<a href="' . htmlspecialchars($nav['link']) . '"><li>' . htmlspecialchars(strtoupper($nav['name'])) . '</li></a><div class="vr"></div>';
		}
	}
	
	return $return;
}

?>

<div class="navigation">
    <div class="navigation_links">
        <div class="container">
            <ul>
            	<a href="<?php echo Config::PATH; ?>"><li id="navi_logo"></li></a><div class="vr"></div>
                <?php echo drawNavi(); ?>
            </ul>
        </div>
    </div>
</div>