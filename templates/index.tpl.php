
<link rel="stylesheet" href="style/index.css">

<!-- Slider Content -->
<div id="slider" class="bigimg" style="overflow:hidden; position:relative;">
    <div class="container" style="left:50%; margin-left:-480px; position:absolute; z-index:999;">
        <div class="left_slider_content">
            <div class="left_headtitle">
                <p>Wir sind für neues offen. <br />
                Trete unserem Clan noch heute bei!</p>
            </div>   
            
            <div class="left_subtext">
                <p>Der <b style="color:#61a8ff;">GamerCrewClan</b> ist eine zusammenkunft allersamt Spieler, <br />
                die Gaming als Kulturgut ansieht und dabei mehr empfindet. Wir sind <br /> eine stark anwachsende
                Community die auf dich wartet. Trete der GamerCrewClan Community bei! </p>
            </div>
            
            <div class="left_slider_button">
                <a href="<?php echo Config::PATH; ?>/register" class="reg_button">Jetzt ein Teil werden</a>
                <a href="<?php echo Config::PATH; ?>/login" class="reg_button" style="width:150px;">Einloggen</a>
            </div>
        </div>
    </div>
</div>

<!-- Main -->
<div class="container">
    <div class="content">
    	<!-- Welcome Text -->
        <div class="content_head"><p>Seit über 3 Jahren begleiten wir dich!</p></div>
        
        <div class="content_subtext">
            <p>Der GamerCrewClan ist bereits seit 3 Jahren existierend. Du und ich, wir starten 2016 erst richtig durch! Nun mit <b>neuer Homepage</b>, <b>neuem System</b> und <b>mehr Events</b>
            sind wir der Clan, für den Gaming und Spaß am vorderster Stelle steht. Schließe dich der großen Vielzahl an Bereichen an die sich unter den beliebstesten Spielen innerhalb des
            Clans seit 3 Jahren aufgebaut haben und werde ein Teil der GamerCrewClan Multigaming Community. <b>Wir sind <b style="color:#3A74BC;">GamerCrewClan</b></b>, bist <b>Du</b> es auch?<b</p>
        </div>
        
        <div class="linie"></div>
        
        <!-- News -->
        <div class="content_head">
            <p>Aktuelle News</p><br />
            <?php 
		$news = new News($connection);
                $data = $news->loadNews(0, 5);
                foreach($data as $article) {
                    
            ?>
            <div class="nikosmama" style="max-width:960px;">
                <div class="thumbnail" style="float:left; margin-right:5px; background:url(<?php echo $article['image']; ?>) center;">
                    <div class="news_title"><a href="<?php echo Config::PATH; ?>/news?newsid=<?php echo $article['id']; ?>"><?php echo $article['title']; ?></a></div>
                    <div class="news_date"><?php echo $article['date']; ?></div>
                </div>
            </div>
            <?php } ?>
        </div>
        
        <div style="clear:both;"></div>
        
        <div class="linie"></div>
        <!-- Statistics -->
        <div class="content_head">
            <p style="float:left;">Statistik</p>
        </div>
        <div style="clear:both;"></div>
        <?php 			
			$stats = new General($connection); 
	    ?>
        
        <div class="index_stats">
        	<div>
            	<span class="glyphicon glyphicon-user"></span> <b>Mitglieder</b><br>
                <p><?php echo htmlspecialchars($stats->statistic("users", "WHERE ID = :id", ['id' => 1]));?></p>
            </div>

        	<div>
            	<span class="glyphicon glyphicon-pencil"></span> <b>Artikel</b><br>
                <p>10</p>
            </div>


        	<div>
            	<span class="glyphicon glyphicon-globe"></span> <b>Online</b><br>
                <p>10</p>
            </div>
        </div>
    </div>
</div>


